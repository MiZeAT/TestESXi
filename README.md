# TestESXi

VMware vSphere ESXi Memory/CPU/Stresstest

## purpose

create and start a memory Test VM per physical CPU core,
calculate and set VM-memory 
(ESXi host memory / # of cores)

## prerequisites

* ESXihost with no running VMs
* workload ISO from (http://www.memtest.org/download/4.20/memtest86+-4.20.iso.zip)


