## TestESXi
#VMware vSphere ESXi Memory/CPU/Stresstest

$myHost="esx2.test.local"                       # Full name no wildcard e.g. esx2.test.local
#$myTestVMName="mem001"
$myTestVMNameBase=($myHost.Split("."))[0]    	#e.g. esx2
$myTestVMName=($myHost.Split("."))[0]+"-001"    #e.g. esx2-001
$ESXiReservd=32*1024

###############################################################

#read vmhost data - CPUs and memory
$myVMhost = Get-VMHost -Name $myHost
$NumCPUs=$MyVMhost.NumCpu

#Calculate per VM memory
$MemoryMB=[System.Math]::Round(($MyVMhost.MemoryTotalMB),0)
$MemoryMB=$MemoryMB-$ESXiReservd
$MemoryperVM=$MemoryMB/$NumCPUs
$MemoryperVM=[System.Math]::Round(($MemoryperVM),0)
#modulo4?
if ($MemoryperVM%4 -ne 0) {$MemoryperVM=$MemoryperVM-1}
if ($MemoryperVM%4 -ne 0) {$MemoryperVM=$MemoryperVM-1}
if ($MemoryperVM%4 -ne 0) {$MemoryperVM=$MemoryperVM-1}

#get datastore
$myDS=$myVMhost| Get-Datastore |Sort-Object FreeSpaceGB -Descending |Select-Object -First 1

#create first VM
Import-VApp -Source ./memtest.ovf -Name $myTestVMName -VMHost $myVMhost -Datastore $myDS
$myTestVM=Get-VM -Name $myTestVMName

#set memory
$myTestVM |Set-VM -MemoryMB $MemoryperVM -Confirm:$false

##set memory reservation to avoid *swp-file-size
$myTestVM | Get-VMResourceConfiguration |Set-VMResourceConfiguration -MemReservationMB $MemoryperVM

#upload ISO to datastore
$myDS | New-DatastoreDrive -Name myDS
Copy-DatastoreItem -Item .\memtest.iso -Destination myDS:\memtest.iso
# set boot-from-ISO
$myTestVM| Get-CDDrive | Set-CDDrive -StartConnected:$true -IsoPath "[$myDS] memtest.iso" -Confirm:$false

# clone VMs
$vmcounter = 002..$NumCPUs
$vmcounter |ForEach-Object { New-VM -Name ($myTestVMNameBase + "-" + "{0:000}" -f $_) -VM $myTestVMName -VMHost $myVMhost}

#check that no VMs are running
if (($MyVMhost |Get-VM |where PowerState -NE "PoweredOff").Count -eq 0) 
{
    Write-Output "No VMs are running ..."
    Write-Output "Starting VMs ..."
    $myVMhost |Get-VM -Name $myTestVMNameBase-??? |Sort-Object Name | foreach {$_ | Start-VM}
}
